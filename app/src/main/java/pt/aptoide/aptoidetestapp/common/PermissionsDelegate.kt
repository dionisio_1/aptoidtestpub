package pt.aptoide.aptoidetestapp.common

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

class PermissionsDelegate(private val activity: Activity) {

    private val tag: String = this.javaClass.simpleName

    companion object {
        const val MULTIPLE_PERMISSIONS_ID = 2000
    }

    fun hasPermissions(permissions: Array<String>): Boolean {

        var needPermissions = true

        permissions.forEach { permission ->

            needPermissions = needPermissions && when (permission) {
                Manifest.permission.ACCESS_COARSE_LOCATION -> (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED)
                Manifest.permission.ACCESS_FINE_LOCATION -> (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED)
                Manifest.permission.INTERNET -> (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.INTERNET
                ) != PackageManager.PERMISSION_GRANTED)

                else -> true
            }
        }
        Log.i(
            tag,
            "Check need permissions ${
                permissions.joinToString(
                    separator = ",",
                    prefix = "[",
                    postfix = "]"
                )
            } is  $needPermissions"
        )
        return needPermissions
    }

    fun requestPermissions(
        fragment: Fragment? = null,
        permissions: Array<String>,
        id: Int = MULTIPLE_PERMISSIONS_ID
    ) {
        Log.i(
            tag,
            "Request need permissions ${
                permissions.joinToString(
                    separator = ",",
                    prefix = "[",
                    postfix = "]"
                )
            }"
        )
        if (fragment == null) {
            ActivityCompat.requestPermissions(activity, permissions, id)
        } else {
            fragment.requestPermissions(permissions, id)
        }
    }
}
