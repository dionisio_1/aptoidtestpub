package pt.aptoide.aptoidetestapp

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import pt.aptoide.aptoidetestapp.repository.AptoideRepository

class MyApplication : Application() {

    private lateinit var applicationScope: CoroutineScope

    val aptoideRepository by lazy {
        AptoideRepository(this)
    }

    override fun onCreate() {
        super.onCreate()
        applicationScope = CoroutineScope(SupervisorJob())
    }
}