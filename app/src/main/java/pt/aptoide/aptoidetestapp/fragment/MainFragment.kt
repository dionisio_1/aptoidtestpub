package pt.aptoide.aptoidetestapp.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import pt.aptoide.aptoidetestapp.MyApplication
import pt.aptoide.aptoidetestapp.adapter.RecyclerViewAptoideDataAdapter
import pt.aptoide.aptoidetestapp.adapter.RecyclerViewAptoideDataLocalAdapter
import pt.aptoide.aptoidetestapp.databinding.FragmentMainBinding
import pt.aptoide.aptoidetestapp.viewmodel.AptoideViewModel

class MainFragment : BaseFragment(), BaseFragmentInterface {

    private val tagName = this.javaClass.simpleName

    private var _binding: FragmentMainBinding? = null
    private lateinit var progressJob: Job
    private lateinit var aptoideAppDataJob: Job
    private lateinit var aptoideAppDataLocalJob: Job
    private lateinit var noInternetAlertJob: Job

    // This property is only valid between onCreateView and onDestroyView.
    private val binding get() = _binding
    private val aptoideViewModel: AptoideViewModel by viewModels {
        viewModelFactory {
            AptoideViewModel(
                (activity?.application as MyApplication).aptoideRepository
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(tagName, "onCreate")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.d(tagName, "onCreateView")
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iniView(view)
        addListeners()
        populateData()

        Log.d(tagName, "onViewCreated")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(tagName, "onAttach")
    }

    override fun onDetach() {
        super.onDetach()
        Log.d(tagName, "onDetach")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(tagName, "onDestroyView")
        _binding = null
    }


    override fun iniView(view: View) {
        binding!!.fragmentMainRecyclerView.setHasFixedSize(false)
        binding!!.fragmentMainRecyclerView.adapter = RecyclerViewAptoideDataAdapter(arrayListOf())
        binding!!.fragmentMainSecondRecyclerView.setHasFixedSize(false)
        binding!!.fragmentMainSecondRecyclerView.adapter =
            RecyclerViewAptoideDataLocalAdapter(arrayListOf())
    }

    override fun onStart() {
        super.onStart()
        collectData()
    }

    override fun onStop() {
        stopCollectData()
        super.onStop()
    }

    override fun collectData() {
        progressJob = lifecycleScope.launch {
            try {
                aptoideViewModel.progress.collect {
                    if (it) {
                        binding!!.fragmentMainProgressBarMain.visibility = View.VISIBLE
                    } else {
                        CoroutineScope(Dispatchers.Main).launch {
                            binding?.fragmentMainProgressBarMain?.visibility = View.GONE
                        }
                    }
                }
            } catch (e: Exception) {
                Log.d(tagName, e.message ?: "", e)
            }
        }
        aptoideAppDataJob = lifecycleScope.launch {
            try {
                aptoideViewModel.aptoideAppData.collect {

                    (binding!!.fragmentMainRecyclerView.adapter as RecyclerViewAptoideDataAdapter).setData(
                        it.responses?.listApps?.datasets?.all?.data?.list
                    )

                }
            } catch (e: Exception) {
                Log.d(tagName, e.message ?: "", e)
            }
        }

        aptoideAppDataLocalJob = lifecycleScope.launch {
            try {
                aptoideViewModel.aptoideAppDataLocal.collect {
                    (binding!!.fragmentMainSecondRecyclerView.adapter as RecyclerViewAptoideDataLocalAdapter).setData(
                        it.responses?.listApps?.datasets?.all?.data?.list
                    )
                }
            } catch (e: Exception) {
                Log.d(tagName, e.message ?: "", e)
            }
        }

        noInternetAlertJob = lifecycleScope.launch {
            try {
                aptoideViewModel.noInternetAlert.collect {
                    binding!!.fragmentMainInternetAlertTextView.visibility =
                        if (it) View.VISIBLE else View.GONE

                    binding!!.fragmentMainMainTitleTextView.visibility =
                        if (it) View.GONE else View.VISIBLE
                    binding!!.fragmentMainRecyclerViewSwipeRefreshLayout.visibility =
                        if (it) View.GONE else View.VISIBLE
                    binding!!.fragmentMainSecondTitleTextView.visibility =
                        if (it) View.GONE else View.VISIBLE
                    binding!!.fragmentMainSecondRecyclerViewSwipeRefreshLayout.visibility =
                        if (it) View.GONE else View.VISIBLE
                }
            } catch (e: Exception) {
                Log.d(tagName, e.message ?: "", e)
            }
        }
    }

    override fun stopCollectData() {
        progressJob.cancel()
        aptoideAppDataJob.cancel()
        aptoideAppDataLocalJob.cancel()
        noInternetAlertJob.cancel()
    }

    override fun addListeners() {
        binding!!.fragmentMainRecyclerViewSwipeRefreshLayout.setOnRefreshListener { // Your code to refresh the list here.
            getData()
        }
        binding!!.fragmentMainRecyclerViewSwipeRefreshLayout.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
        binding!!.fragmentMainSecondRecyclerViewSwipeRefreshLayout.setOnRefreshListener { // Your code to refresh the list here.
            getDataLocal()
        }
        binding!!.fragmentMainSecondRecyclerViewSwipeRefreshLayout.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )
    }

    override fun populateData() {
        getData()
        getDataLocal()
    }

    private fun getData() {
        binding!!.fragmentMainProgressBarMain.visibility = View.VISIBLE
        CoroutineScope(Dispatchers.Main).launch {
            aptoideViewModel.getAptoideData()
        }
    }

    private fun getDataLocal() {
        binding!!.fragmentMainProgressBarMain.visibility = View.VISIBLE
        CoroutineScope(Dispatchers.Main).launch {
            aptoideViewModel.getAptoideDataLocal()
        }
    }

}