package pt.aptoide.aptoidetestapp.fragment

import android.view.View

interface BaseFragmentInterface {
    fun iniView(view: View)
    fun addListeners()
    fun populateData()
    fun collectData()
    fun stopCollectData()
}
