package pt.aptoide.aptoidetestapp.drawable

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable

object MyCircularProgressDrawable {

    fun getDrawable(context: Context): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        return circularProgressDrawable
    }
}
