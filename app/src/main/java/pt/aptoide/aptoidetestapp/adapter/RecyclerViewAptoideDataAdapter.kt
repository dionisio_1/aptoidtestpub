package pt.aptoide.aptoidetestapp.adapter

import pt.aptoide.aptoidetestapp.repository.dao.AptoideApp
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pt.aptoide.aptoidetestapp.R
import pt.aptoide.aptoidetestapp.databinding.GraphicCardRecyclerviewItemBinding
import pt.aptoide.aptoidetestapp.drawable.MyCircularProgressDrawable

class RecyclerViewAptoideDataAdapter(private var itemList: List<AptoideApp>) :
    RecyclerView.Adapter<RecyclerViewAptoideDataAdapter.AptoideDataHolder>() {

    class AptoideDataHolder(
        private val itemBinding: GraphicCardRecyclerviewItemBinding,
        private val context: Context
    ) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(adapter: RecyclerViewAptoideDataAdapter, aptoideAppItem: AptoideApp) {

            itemBinding.graphicCardRecyclerviewItemTitleTextView.text = aptoideAppItem.name
            itemBinding.graphicCardRecyclerviewItemRatingTextView.text =
                aptoideAppItem.rating.toString()
            itemBinding.graphicCardRecyclerviewItemRatingRatingBar.rating =
                aptoideAppItem.rating.toFloat()

            CoroutineScope(Dispatchers.Main).launch {
                delay(100)
                Glide.with(context)
                    .load(aptoideAppItem.graphic)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(MyCircularProgressDrawable.getDrawable(context))
                    .error(R.drawable.aptoide_error)
                    .into(itemBinding.graphicCardRecyclerviewItemBannerImageView)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AptoideDataHolder {
        val itemBinding = GraphicCardRecyclerviewItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return AptoideDataHolder(itemBinding, parent.context)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: AptoideDataHolder, position: Int) {
        val aptoideApp: AptoideApp = itemList[position]
        holder.bind(this, aptoideApp)
    }

    fun setData(list: List<AptoideApp>?) {
        list?.let {
            itemList = list
            notifyDataSetChanged()
        }
    }


}
