package pt.aptoide.aptoidetestapp.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import pt.aptoide.aptoidetestapp.repository.AptoideRepository
import pt.aptoide.aptoidetestapp.repository.BaseRetrofitRepository
import pt.aptoide.aptoidetestapp.repository.dao.AptoideAppDataCallResponse

class AptoideViewModel(
    private val aptoideRepository: AptoideRepository
) : ViewModel() {

    private val aptoideAppDataCallResponseInitState = AptoideAppDataCallResponse("", null)
    private val tagName = this.javaClass.simpleName
    private val _aptoideAppData: MutableStateFlow<AptoideAppDataCallResponse> =
        MutableStateFlow(aptoideAppDataCallResponseInitState)
    private val _aptoideAppDataLocal: MutableStateFlow<AptoideAppDataCallResponse> =
        MutableStateFlow(aptoideAppDataCallResponseInitState)
    private val _noInternetAlert: MutableStateFlow<Boolean> = MutableStateFlow(false)
    private val _progress: MutableStateFlow<Boolean> = MutableStateFlow(false)

    val aptoideAppData: StateFlow<AptoideAppDataCallResponse> = _aptoideAppData
    val aptoideAppDataLocal: StateFlow<AptoideAppDataCallResponse> = _aptoideAppDataLocal
    val noInternetAlert: StateFlow<Boolean> = _noInternetAlert
    val progress: StateFlow<Boolean> = _progress

    fun getAptoideData() {
        _progress.value = true
        val data = aptoideRepository.latestAppsRx
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                try {
                    if (it.first.isNotEmpty() && it.first[0] == BaseRetrofitRepository.NO_INTERNET_ERROR) {
                        _noInternetAlert.value = true
                    } else {
                        _noInternetAlert.value = false
                        it.second?.let { sec ->
                            _aptoideAppData.value = sec
                        }
                        Log.d(
                            tagName,
                            "Aptoide latestAppsRx call Status:${it.second?.status}"
                        )
                    }
                    _progress.value = false
                } catch (e: Exception) {
                    Log.d(tagName, e.message ?: "", e)
                }
            }
    }

    fun getAptoideDataLocal() {
        viewModelScope.launch(Dispatchers.IO) {
            _progress.value = true
            //With kotlin Flow
            try {
                aptoideRepository.latestApps.collect {
                    if (it.first.isNotEmpty() && it.first[0] == BaseRetrofitRepository.NO_INTERNET_ERROR) {
                        _noInternetAlert.value = true
                    } else {
                        _noInternetAlert.value = false
                        it.second?.let { res ->
                            _aptoideAppDataLocal.value = res
                        }
                        Log.d(
                            tagName,
                            "Aptoide latestApps call Status:${it.second?.status}"
                        )
                    }
                    _progress.value = false
                }
            } catch (e: CancellationException) {
                Log.d(tagName, e.message ?: "", e)
            }
        }
    }
}