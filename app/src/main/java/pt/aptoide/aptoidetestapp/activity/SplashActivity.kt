package pt.aptoide.aptoidetestapp.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import pt.aptoide.aptoidetestapp.common.PermissionsDelegate

class SplashActivity : AppCompatActivity() {

    private val tagName = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(tagName, "onCreate")

        val permissionsDelegate = PermissionsDelegate(this)
        val permissions = arrayOf(
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (permissionsDelegate.hasPermissions(permissions)) {
            permissionsDelegate.requestPermissions(permissions = permissions)
        } else {
            accessApp()
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(tagName, "onRequestPermissionsResult")

        if (grantResults.isNotEmpty() && !grantResults.contains(PackageManager.PERMISSION_DENIED)) {
            if (requestCode == PermissionsDelegate.MULTIPLE_PERMISSIONS_ID) {
                accessApp()
            }
        } else {
            Log.d(tagName, "Permissions Denied!")
        }
    }

    private fun accessApp() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(3000)
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}