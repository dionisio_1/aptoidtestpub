package pt.aptoide.aptoidetestapp.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import pt.aptoide.aptoidetestapp.R
import pt.aptoide.aptoidetestapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val tagName = this.javaClass.simpleName

    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(tagName, "onCreate")
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupNavigation()
    }

    private fun setupNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.ActivityMainNavHostFragmentContainerView) as NavHostFragment
        navController = navHostFragment.navController

        binding.ActivityMainBottomNavigationView.setupWithNavController(navController)
        binding.ActivityMainBottomNavigationView.setOnItemSelectedListener { menuItem ->
            menuItem.isChecked = true

            when (menuItem.itemId) {

                R.id.menuOne -> navController.navigate(R.id.mainFragment)

                R.id.menuTwo -> navController.navigate(R.id.appsFragment)

                else -> navController.navigate(R.id.mainFragment)
            }
            true
        }
    }

}