package pt.aptoide.aptoidetestapp.repository

import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

open class BaseRetrofitRepository(private val context: Context?) {
    private val tagName = this.javaClass.simpleName

    private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()
    var client: OkHttpClient

    internal var gson = GsonBuilder()
        .setLenient()
        .create()

    companion object {
        const val NO_INTERNET_ERROR = "NO INTERNET ACCESS"
        const val SERVICE_ERROR = "SERVICE_ERROR"
    }

    init {
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        Log.d(tagName, "Initialized")
    }

    internal fun checkNoInternetAccess() =
        if (context == null) false else !EssentialNetworkUtil.hasInternetWifi(context)

}
