package pt.aptoide.aptoidetestapp.repository

import io.reactivex.Observable
import pt.aptoide.aptoidetestapp.repository.dao.AptoideAppDataCallResponse
import retrofit2.http.GET

interface AptoideAPI {
    companion object {
        internal const val aptoideApiBaseUrl: String = "http://ws2.aptoide.com/api/6/"
    }

    @GET("bulkRequest/api_list/listApps")
    fun getBulkRequestListAppsSync(): Observable<AptoideAppDataCallResponse>

    @GET("bulkRequest/api_list/listApps")
    suspend fun getBulkRequestListApps(): AptoideAppDataCallResponse
}
