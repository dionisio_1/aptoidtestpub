package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class AptoideApp(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("package") val aptoidePackage: String,
    @SerializedName("store_id") val store_id: Int,
    @SerializedName("store_name") val store_name: String,
    @SerializedName("vername") val vername: String,
    @SerializedName("vercode") val vercode: Int,
    @SerializedName("md5sum") val md5sum: String,
    @SerializedName("apk_tags") val apk_tags: List<String>,
    @SerializedName("size") val size: Int,
    @SerializedName("downloads") val downloads: Int,
    @SerializedName("pdownloads") val pdownloads: Int,
    @SerializedName("added") val added: String,
    @SerializedName("modified") val modified: String,
    @SerializedName("updated") val updated: String,
    @SerializedName("rating") val rating: Double,
    @SerializedName("icon") val icon: String,
    @SerializedName("graphic") val graphic: String,
    @SerializedName("uptype") val uptype: String
)