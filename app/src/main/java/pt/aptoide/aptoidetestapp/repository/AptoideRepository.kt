package pt.aptoide.aptoidetestapp.repository

import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pt.aptoide.aptoidetestapp.repository.dao.AptoideAppDataCallResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class AptoideRepository(context: Context?) : BaseRetrofitRepository(context) {

    private val tagName = this.javaClass.simpleName
    private var retrofitAptoideBuilder = Retrofit.Builder()
        .baseUrl(AptoideAPI.aptoideApiBaseUrl)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    private var retrofitRxAptoideBuilder = Retrofit.Builder()
        .baseUrl(AptoideAPI.aptoideApiBaseUrl)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    private var aptoideService: AptoideAPI = retrofitAptoideBuilder.create(AptoideAPI::class.java)

    private var aptoideRxService: AptoideAPI =
        retrofitRxAptoideBuilder.create(AptoideAPI::class.java)

    companion object {
        private const val REPOSITORY_ERROR = "REPOSITORY_ERROR"
    }

    init {
        Log.d(tagName, "Initialized")
    }

    /**
     * With KotlinFlow
     */
    val latestApps: Flow<Pair<Array<String>, AptoideAppDataCallResponse?>> = flow {
        try {
            val latestNews: Pair<Array<String>, AptoideAppDataCallResponse?> =

                if (checkNoInternetAccess()) {
                    Pair(arrayOf(NO_INTERNET_ERROR), null)
                } else {
                    val apps: AptoideAppDataCallResponse = aptoideService.getBulkRequestListApps()
                    Pair(arrayOf(), apps)
                }
            emit(latestNews)
        } catch (e: java.lang.Exception) {
            emit(Pair(arrayOf(REPOSITORY_ERROR), null))
        }
    }

    /**
     * With JavaRx
     */
    val latestAppsRx: Observable<Pair<Array<String>, AptoideAppDataCallResponse?>> =
        Observable.create { emitter ->
            try {
                if (checkNoInternetAccess()) {
                    emitter.onNext(Pair(arrayOf(NO_INTERNET_ERROR), null))
                    emitter.onComplete()
                } else {
                    aptoideRxService.getBulkRequestListAppsSync()
                        .repeat()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(object : Observer<AptoideAppDataCallResponse> {

                            override fun onError(e: Throwable) {
                                emitter.onNext(Pair(arrayOf(SERVICE_ERROR), null))
                                emitter.onComplete()
                            }

                            override fun onNext(t: AptoideAppDataCallResponse) {
                                val code: Array<String> = arrayOf()
                                val latestNews = Pair(code, t)
                                emitter.onNext(latestNews)
                                emitter.onComplete()
                            }

                            override fun onSubscribe(d: Disposable) {
                            }

                            override fun onComplete() {
                            }

                        })
                }
            } catch (e: Exception) {
                emitter.onNext(Pair(arrayOf(REPOSITORY_ERROR), null))
                emitter.onComplete()
            }
        }

}