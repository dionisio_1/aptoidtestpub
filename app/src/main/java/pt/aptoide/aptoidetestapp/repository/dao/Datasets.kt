package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class Datasets(

    @SerializedName("all") val all: All
)