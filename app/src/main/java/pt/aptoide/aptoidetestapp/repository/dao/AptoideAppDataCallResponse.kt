package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class AptoideAppDataCallResponse(
    @SerializedName("status") val status: String,
    @SerializedName("responses") val responses: Responses?
)
