package pt.aptoide.aptoidetestapp.repository

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log

object EssentialNetworkUtil {

    private const val NONE = 0
    private const val MOBILE = 1
    private const val WIFI = 2
    private const val VPN = 3
    private const val BLUETOOTH = 4
    private const val ETHERNET = 5
    private const val WIFI_AWARE = 6
    private const val LOWPAN = 7

    private val tagName = this.javaClass.simpleName

    /**
     * // Returns connection type. 0: none; 1: mobile data; 2: wifi; 3: VPN
     */
    private fun getConnectionType(context: Context): Array<Int> {
        Log.d(tagName, "getConnectionType - START")
        val result: MutableList<Int> = mutableListOf()
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val allNetworks = cm?.allNetworks

        allNetworks?.forEach { network ->

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cm.getNetworkCapabilities(network)?.let { netCap ->
                    val capabilities = when {
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> MOBILE
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> BLUETOOTH
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> ETHERNET
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> VPN
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI_AWARE) -> WIFI_AWARE
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_LOWPAN) -> LOWPAN
                        netCap.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> WIFI
                        else -> NONE
                    }
                    if (!result.contains(capabilities)) {
                        result.add(capabilities)
                    }
                }

            } else {
                cm.activeNetworkInfo?.let {
                    val capabilities = when (it.type) {
                        ConnectivityManager.TYPE_WIFI -> WIFI
                        ConnectivityManager.TYPE_MOBILE -> MOBILE
                        ConnectivityManager.TYPE_VPN -> VPN
                        else -> NONE
                    }
                    if (!result.contains(capabilities)) {
                        result.add(capabilities)
                    }
                }

            }
        }
        if (result.isEmpty()) {
            result.add(NONE)
        }
        Log.d(tagName, "getConnectionType - END [$result]")
        return result.toTypedArray()
    }

    fun hasInternetWifi(context: Context): Boolean =
        getConnectionType(context).contains(MOBILE) ||
                getConnectionType(context).contains(WIFI)
}
