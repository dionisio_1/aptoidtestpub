package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class Data(

    @SerializedName("total") val total: Int,
    @SerializedName("offset") val offset: Int,
    @SerializedName("limit") val limit: Int,
    @SerializedName("next") val next: Int,
    @SerializedName("hidden") val hidden: Int,
    @SerializedName("list") val list: List<AptoideApp>
)