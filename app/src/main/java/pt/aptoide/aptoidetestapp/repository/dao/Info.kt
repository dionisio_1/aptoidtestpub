package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class Info(

    @SerializedName("status") val status: String,
    @SerializedName("time") val time: Time
)