package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class All(

    @SerializedName("info") val info: Info,
    @SerializedName("data") val data: Data
)