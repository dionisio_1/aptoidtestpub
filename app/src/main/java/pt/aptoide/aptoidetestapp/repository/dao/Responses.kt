package pt.aptoide.aptoidetestapp.repository.dao

import com.google.gson.annotations.SerializedName

data class Responses(

    @SerializedName("listApps") val listApps: ListApps
)