package pt.aptoide.aptoidetestapp

import android.util.Log
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import pt.aptoide.aptoidetestapp.repository.AptoideRepository

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class RepositoryTest {
    private val tagName = this.javaClass.simpleName
    private lateinit var repository: AptoideRepository

    @Before
    fun setup() {
        repository = AptoideRepository(null)
    }

    @Test
    fun a001_AptoideRepositoryKotlinFlowTest() {
        runBlocking {
            Log.d(tagName, "[START] - Launch Test: a001_AptoideRepositoryKotlinFlowTest")
            repository.latestApps.collect {

                Assert.assertArrayEquals(arrayOf(), it.first)
                Assert.assertNotNull(it.second)
                Assert.assertNotNull(it.second?.status, "OK")
            }
            Log.d(tagName, "[END] - Launch Test: a001_AptoideRepositoryKotlinFlowTest")
        }

    }

    @Test
    fun a002_AptoideRepositoryJavaRx2Test() {
        Log.d(tagName, "[START] - Launch Test: a002_AptoideRepositoryJavaRx2Test")
        repository.latestAppsRx
            .subscribeOn(Schedulers.io())
            .subscribe {
                Assert.assertArrayEquals(arrayOf(), it.first)
                Assert.assertNotNull(it.second)
                Assert.assertNotNull(it.second?.status, "OK")
            }
        Log.d(tagName, "[END] - Launch Test: a002_AptoideRepositoryJavaRx2Test")
    }
}