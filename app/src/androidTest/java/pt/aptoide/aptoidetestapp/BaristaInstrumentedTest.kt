package pt.aptoide.aptoidetestapp

import android.Manifest
import android.util.Log
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions
import com.schibsted.spain.barista.interaction.BaristaMenuClickInteractions
import com.schibsted.spain.barista.interaction.BaristaSleepInteractions
import com.schibsted.spain.barista.interaction.PermissionGranter
import org.junit.*
import org.junit.runners.MethodSorters
import pt.aptoide.aptoidetestapp.activity.SplashActivity
import java.util.concurrent.TimeUnit

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class BaristaInstrumentedTest {

    private val tagName = this.javaClass.simpleName
    private lateinit var activityIds: Array<Int>
    private lateinit var fragmentMainDisplayedIds: Array<Int>
    private lateinit var fragmentMainNotDisplayedIds: Array<Int>
    private lateinit var fragmentAppsDisplayedIds: Array<Int>
    private lateinit var fragmentAppsNotDisplayedIds: Array<Int>
    private lateinit var graphicCardRecyclerviewItemIds: Array<Int>
    private lateinit var iconCardRecyclerviewItemIds: Array<Int>

    @get:Rule
    private var personalBaristaRule: PersonalBaristaRule<SplashActivity> =
        PersonalBaristaRule.create(SplashActivity::class.java)

    companion object {
        fun getPermissions() {
            PermissionGranter.allowPermissionsIfNeeded(Manifest.permission.INTERNET)
            BaristaSleepInteractions.sleep(2000)
        }
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        activityIds = arrayOf(
            R.id.ActivityMainRootMerge,
            R.id.ActivityMainNavHostFragmentContainerView,
            R.id.ActivityMainBottomNavigationView
        )
        fragmentMainDisplayedIds = arrayOf(
            R.id.fragmentMainBaseConstraintLayout,
            R.id.fragmentMainMainTitleTextView,
            R.id.fragmentMainRecyclerViewSwipeRefreshLayout,
            R.id.fragmentMainRecyclerView,
            R.id.fragmentMainSecondTitleTextView,
            R.id.fragmentMainSecondRecyclerViewSwipeRefreshLayout,
            R.id.fragmentMainSecondRecyclerView
        )
        fragmentMainNotDisplayedIds = arrayOf(
            R.id.fragmentMainInternetAlertTextView,
            R.id.fragmentMainProgressBarMain
        )
        fragmentAppsDisplayedIds = arrayOf(
            R.id.fragmentAppsBaseConstraintLayout,
            R.id.fragmentAppsRecyclerViewSwipeRefreshLayout,
            R.id.fragmentAppsRecyclerView
        )
        fragmentAppsNotDisplayedIds = arrayOf(
            R.id.fragmentAppsInternetAlertTextView,
            R.id.fragmentAppsProgressBarMain
        )
        graphicCardRecyclerviewItemIds = arrayOf(
            R.id.graphicCardRecyclerviewItemBaseCardView,
            R.id.graphicCardRecyclerviewItemBaseConstraintLayout,
            R.id.graphicCardRecyclerviewItemBannerImageView,
            R.id.graphicCardRecyclerviewItemTitleTextView,
            R.id.graphicCardRecyclerviewItemRatingRatingBar,
            R.id.graphicCardRecyclerviewItemRatingTextView
        )
        iconCardRecyclerviewItemIds = arrayOf(
            R.id.iconCardRecyclerviewItemBaseCardView,
            R.id.iconCardRecyclerviewItemBaseConstraintLayout,
            R.id.iconCardRecyclerviewItemIconImageView,
            R.id.iconCardRecyclerviewItemTitleTextView,
            R.id.iconCardRecyclerviewItemRatingImageView,
            R.id.iconCardRecyclerviewItemRatingTextView
        )
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
    }

    @Test
    @Throws(Exception::class)
    fun a001AptoideNavigationTest() {
        Log.d(tagName, "[START] - Launch Test: a1LoginTest")

        personalBaristaRule.launchActivity()
        getPermissions()
        BaristaSleepInteractions.sleep(5, TimeUnit.SECONDS)

        testMainFragment()

        BaristaMenuClickInteractions.clickMenu("Apps")
        BaristaSleepInteractions.sleep(2, TimeUnit.SECONDS)

        testAppsFragment()

        BaristaMenuClickInteractions.clickMenu("Home")
        BaristaSleepInteractions.sleep(2, TimeUnit.SECONDS)

        testMainFragment()

        Log.d(tagName, "[FINISH] - Test: a1LoginTest")
    }

    private fun testAppsFragment() {
        fragmentMainDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotExist(it)
        }
        fragmentMainNotDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotExist(it)
        }
        fragmentAppsDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
        fragmentAppsNotDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotDisplayed(it)
        }
        graphicCardRecyclerviewItemIds.forEach {
            BaristaVisibilityAssertions.assertNotExist(it)
        }
        iconCardRecyclerviewItemIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
    }

    private fun testMainFragment() {
        activityIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
        fragmentMainDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
        fragmentMainNotDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotDisplayed(it)
        }
        fragmentAppsDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotExist(it)
        }
        fragmentAppsNotDisplayedIds.forEach {
            BaristaVisibilityAssertions.assertNotExist(it)
        }
        graphicCardRecyclerviewItemIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
        iconCardRecyclerviewItemIds.forEach {
            BaristaVisibilityAssertions.assertDisplayed(it)
        }
    }
}