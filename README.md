# Aptoide test App #
[![](https://img.shields.io/badge/Version-1.0.0-blue.svg?style=flat)]()
[![](https://img.shields.io/badge/platform-android-green.svg)](http://developer.android.com/index.html)
[![](https://img.shields.io/badge/API-21%2B-green.svg)](https://android-arsenal.com/api?level=21)

## Instructions ###

The goal of this Challenge is to create a simple Android app that downloads a list of
applications from Aptoide in JSON format, and shows its details inside a RecyclerView /
ListView. The screen to the right is an example for the layout that you can implement. 
The JSON information is to be obtained on the following URL:

[listApps](http://ws2.aptoide.com/api/6/bulkRequest/api_list/listApps)

The minimum SDK version should be 21. All other decision implementations are up to
the candidate, preferably detailed within
comments in the code. We would like to encourage the use of:

* Kotlin
* Retrofit2
* RxJava
* Material design
* An architect pattern of your choice, please add an explanation for your choice
* Tests



## My solution involves the following characteristics:

* SplashActivity for launch and permissions and main activity for all others screens.
* It´s use only viewModels from fragments instantiate/remove on fragments.
* Navigation graph: use of navigation graph for fragment changes using bottom navigation 
* MVVM: isolation of the UI layer and the data layer by using MVVM / Kotlin Flow. 
  > In this special case **I add on of repository calls using JavaRx for demonstration only**.
* Repository to access data from all sources. In this case, access web service using retrofit2  
* Glide: glide lib to load and cache images

##### Architecture
* [MVVM](https://www.avantica.net/blog/mvvm-data-binding-kotlin-efficient-and-easier-code)
##### Libs
* [Androidx](https://developer.android.com/jetpack/androidx)
* [Barista](https://github.com/AdevintaSpain/Barista)
* [Glide](https://github.com/bumptech/glide)
* [Gson](https://github.com/google/gson)
* [Robolectric](http://robolectric.org/getting-started/)
* [RXJava2](https://github.com/ReactiveX/RxJava) 
  > **Used for demonstration only. My default config is usinf Kotlin Flow**
##### Board

##### Status

* Version 1.0
